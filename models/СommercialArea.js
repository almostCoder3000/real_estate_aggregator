var mongoose = require('mongoose'),
  Schema = mongoose.Schema;
var Flat = require('./Flat.js');

var crypto = require('crypto');
var async = require('async');
var config = require('../config.js');
var _ = require('lodash');

var debug = require('debug')('app:server');



var CommercialArea = Flat.Flat.discriminator('CommercialArea', new Schema({
    type: {
        type: String,
        default: "commercial"
    },
    area: {
        from: {
            type: Number
        },
        to: {
            type: Number
        }
    },
    district: {
        type: mongoose.Schema.ObjectId,
        ref: 'Disctrict',
        required: true
    },
    address: {
        street: {
            type: String
        },
        house_number: {
            type: Number
        }
    },
    building_type: {
        type: Object
    }
}, {discriminatorKey: 'kind'}));


exports.CommercialArea = CommercialArea;

var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var crypto = require('crypto');
var async = require('async');
var config = require('../config.js');
var _ = require('lodash');

var debug = require('debug')('app:server');


var schema = new Schema({
    title: {
        type: String,
        required: true
    },
    n1: {
        id: {
            type: Number
        },
        suburbs: {
            type: Array
        }
    },
    domofond: {
        id: {
            type: Number
        },
        latinName: {
            type: String
        },
        region: {
            titleLatin: { type: String },
            id: { type: Number }
        }
    }
});


exports.City = City = mongoose.model('City', schema);

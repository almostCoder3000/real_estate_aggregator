var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var crypto = require('crypto');
var async = require('async');
var config = require('../config.js');
var _ = require('lodash');

var debug = require('debug')('app:server');


var schema = new Schema({
    city: {
        type: mongoose.Schema.ObjectId,
        ref: 'City'
    },
    deal_type: {
        type: String
    },
    n1_id: {
        type: Number
    },
    domofond_id: {
        type: Number
    },
    price: {
        type: Number
    }
}, {discriminatorKey: 'kind'});


exports.Flat = Flat = mongoose.model('Flat', schema);

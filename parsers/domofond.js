let fs = require('fs');
var async = require('async');
var request = require('request-promise');
var City = require('../models/City').City;

class DomofondParser {
    constructor() {
        this.regions = [];
    }

    static async _getRegions() {
        var body = await request("https://www.domofond.ru/prodazha");
        console.log(1);
        var initialSettings;
        try {
            body = body.split("Home.HomeSearchBarControl.renderTo(document.getElementById('")[1];
            body = body.split("),")[1];
            body = body.split(");")[0]
            initialSettings = JSON.parse(body);
        } catch (e) {

        }
        if (initialSettings) {
            var {popularAreas, remainingAreas} = initialSettings.locationSearchBar;
            this.regions = popularAreas.concat(remainingAreas);
            console.log(2);
            this._getCities();
        }
    }

    static async _getCities() {
      console.log(2);
      async.eachSeries(this.regions, async function (area, next) {
          request(`https://www.domofond.ru/shared/areaselectiondialog?AreaId=${area.areaId}&AreaType=Region`);

          try {
              let cities = await htmlToJson.parse(body_cts, function() {
                  return this.map('div.b-checkbox.cityItem', (cityItem) => {
                      return [cityItem.attr("data-id"), cityItem.attr("data-name")];
                  })
              });
          } catch (e) {
              console.error(e)
          }


          cities.forEach((city) => {
              console.log(`for ${area.displayName} ${city[0]} and ${city[1]}\n`);
              City.findOne({title: city[1]}).exec((err, foundCity) => {
                  if (!foundCity) {
                      foundCity = new City({title: city[1]});
                  }
                  foundCity.domofond.id = city[0];
                  foundCity.domofond.latinName = cyrillicToTranslit().transform(city[1], "_").toLowerCase();
                  foundCity.domofond.region.titleLatin = area.latinName;
                  foundCity.domofond.region.id = area.areaId;
                  console.log('City:: ', foundCity.title);
                  //foundCity.save(err => next());
              });
          })
      });
    }

    static execGetCities() {
        console.log(this._getRegions);
        this._getRegions();
    }
}

module.exports = DomofondParser;
